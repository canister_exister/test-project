//
//  SchoolDetailsPresenter.swift
//  NYCSchools
//
//  Created by Vitalii Miroshnychenko on 5/7/20.
//  Copyright © 2020 Vitalii Miroshnychenko. All rights reserved.
//

import Foundation

protocol SchoolDetailsPresenterProtocol : AnyObject {
    func getDateailsFor(school: String?)
}

class SchoolDetailsPresenter: SchoolDetailsPresenterProtocol {
    
    weak var viewController: SchoolDetailsVCProtocol?
    
    private lazy var apiEngine: ApiEngine = {
        return ApiEngine()
    }()
    
    required init(_ viewController: SchoolDetailsVCProtocol) {
        self.viewController = viewController
    }
    
    func getDateailsFor(school: String?) {
        
        guard let id = school else {
            viewController?.showAlert()
            return
        }
        
        let url = SCHOOL_DETAILS_URL + SCHOOL_NAME + id

        apiEngine.createRequest(endPoint: url, type: [SchoolDetailsModel].self) { [weak self] result in
            
            switch result {
            case .success(let details):
                guard let schoolDetails = (details as? [SchoolDetailsModel])?.first else {
                    self?.viewController?.showAlert()
                    return
                }
                
                guard schoolDetails.testTakers != "s" else {
                    self?.viewController?.showAlert()
                    return
                }
                
                self?.viewController?.updatedDetailsFor(math: schoolDetails.mathScore, reading: schoolDetails.readingScore, writing: schoolDetails.writingScore)
    
                return
            case .error(let error):
                print(error)
                self?.viewController?.showAlert()
                
                return
            }
        }
    }
}
