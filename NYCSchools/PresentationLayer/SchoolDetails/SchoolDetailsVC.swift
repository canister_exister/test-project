//
//  SchoolDetailsVC.swift
//  NYCSchools
//
//  Created by Vitalii Miroshnychenko on 5/7/20.
//  Copyright © 2020 Vitalii Miroshnychenko. All rights reserved.
//

import UIKit

protocol SchoolDetailsVCProtocol: AnyObject {
    
    func updatedDetailsFor(math:String, reading: String, writing:String)
    func showAlert()
}

class SchoolDetailsVC: UIViewController {

    @IBOutlet private weak var mathScore: UILabel!
    @IBOutlet private weak var readingScore: UILabel!
    @IBOutlet private weak var writingScore: UILabel!
    
    var presenter: SchoolDetailsPresenterProtocol?
    var schoolID: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = SchoolDetailsPresenter(self)
        presenter?.getDateailsFor(school: schoolID)
    }
}

extension SchoolDetailsVC: SchoolDetailsVCProtocol {
    func updatedDetailsFor(math:String, reading: String, writing:String) {
        performUIUpdate {
            self.mathScore.text = "\(MATH_SCORE) - \(math)"
            self.readingScore.text = "\(READING_SCORE) - \(reading)"
            self.writingScore.text = "\(WRITING_SCORE) - \(writing)"
        }
    }
    
    func showAlert() {
        performUIUpdate {
            let alert = UIAlertController(title: "No scores found", message: "", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { _ in
                self.navigationController?.popViewController(animated: true)
            })
        
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
}
