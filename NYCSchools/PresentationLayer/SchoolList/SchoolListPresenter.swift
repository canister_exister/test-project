//
//  SchoolListPresenter.swift
//  NYCSchools
//
//  Created by Vitalii Miroshnychenko on 5/6/20.
//  Copyright © 2020 Vitalii Miroshnychenko. All rights reserved.
//

import Foundation

protocol SchoolListPresenterProtocol : AnyObject {
    func getSchoolList()
}

class SchoolListPresenter: SchoolListPresenterProtocol {
    
    weak var viewController: SchoolListVCProtocol?
    
    private lazy var apiEngine: ApiEngine = {
        return ApiEngine()
    }()
    
    required init(_ viewController: SchoolListVCProtocol) {
        self.viewController = viewController
    }
    
    func getSchoolList() {
        apiEngine.createRequest(endPoint: SCHOOL_LIST_URL, type: [SchoolListModel].self) { [weak self] result in
            
            switch result {
            case .success(let shools):
                guard let schoolList = shools as? [SchoolListModel] else { return }
                self?.viewController?.schoolList = schoolList
                self?.viewController?.updateSchoolList()
                return
            case .error(let error):
                
                print(error)
                
                return
            }
        }
    }
}
