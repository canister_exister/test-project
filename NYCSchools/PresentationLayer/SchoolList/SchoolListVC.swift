//
//  SchoolListVC.swift
//  NYCSchools
//
//  Created by Vitalii Miroshnychenko on 5/6/20.
//  Copyright © 2020 Vitalii Miroshnychenko. All rights reserved.
//

import UIKit


protocol SchoolListVCProtocol: AnyObject {
    
    var schoolList: [SchoolListModel] { get set }
    func updateSchoolList()
    
}

class SchoolListVC: UIViewController {
            
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    var schoolList = [SchoolListModel]()
    var presenter: SchoolListPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        presenter = SchoolListPresenter(self)
        presenter?.getSchoolList()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let index = tableView.indexPathForSelectedRow{
            tableView.deselectRow(at: index, animated: true)
        }
    }
    
    private func setupUI() {
        
        tableView.register(UINib(nibName: "CustomCell", bundle: nil), forCellReuseIdentifier: "schoolCell")
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let schoolDetail = segue.destination as? SchoolDetailsVC else { return }
        schoolDetail.schoolID = sender as? String
    }
}

extension SchoolListVC: SchoolListVCProtocol {
    
    func updateSchoolList() {
        performUIUpdate {
            self.tableView.reloadData()
            self.activityIndicator.stopAnimating()
        }
    }
}

extension SchoolListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "schoolCell", for: indexPath) as? CustomCell else {
            return UITableViewCell()
        }
        
        cell.schoolNameLabel.text = schoolList[indexPath.row].school_name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "schoolDetailsSegue", sender: schoolList[indexPath.row].dbn)
    }
}
