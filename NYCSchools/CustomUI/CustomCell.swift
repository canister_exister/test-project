//
//  CustomCell.swift
//  NYCSchools
//
//  Created by Vitalii Miroshnychenko on 5/6/20.
//  Copyright © 2020 Vitalii Miroshnychenko. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {

    @IBOutlet weak var schoolNameLabel: UILabel!
    
}
