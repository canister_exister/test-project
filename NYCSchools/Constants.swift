//
//  Constants.swift
//  NYCSchools
//
//  Created by Vitalii Miroshnychenko on 5/6/20.
//  Copyright © 2020 Vitalii Miroshnychenko. All rights reserved.
//


let SCHOOL_LIST_URL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
let SCHOOL_DETAILS_URL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?"
let SCHOOL_NAME = "dbn="
let WRITING_SCORE = "SAT score Writing"
let READING_SCORE = "SAT score Reading"
let MATH_SCORE = "SAT score Math"
