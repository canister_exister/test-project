//
//  PerformUIUpdate.swift
//  NYCSchools
//
//  Created by Vitalii Miroshnychenko on 5/7/20.
//  Copyright © 2020 Vitalii Miroshnychenko. All rights reserved.
//

import UIKit

func performUIUpdate(using closure: @escaping () -> Void) {
    // If we are already on the main thread, execute the closure directly
    if Thread.isMainThread {
        closure()
    } else {
        DispatchQueue.main.async(execute: closure)
    }
}
