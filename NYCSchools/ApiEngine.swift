//
//  ApiEngine.swift
//  NYCSchools
//
//  Created by Vitalii Miroshnychenko on 5/6/20.
//  Copyright © 2020 Vitalii Miroshnychenko. All rights reserved.
//

import Foundation

enum URLResult {
    case response(Data, URLResponse)
    case error(ApiEngineError)
}

enum ApiEngineError: Error {
    case badURL
    case badJSON
    case noConnection
    case apiError(String)
}

enum ApiEngineResult<T> {
    case success(T)
    case error(ApiEngineError)
}

class ApiEngine: NSObject {
   
    private let defaultSession = URLSession(configuration: .default)
    
    private var dataTask: URLSessionDataTask?
        
    func createRequest<T:Decodable>(endPoint:String, type:T.Type, completion:
        @escaping(ApiEngineResult<Any>)->()) {
        
        guard let url = URL(string:endPoint) else {
            completion(.error(.badURL))
            return
        }
        
        let request = URLRequest(url: url)
        
        requestWith(urlRequest: request) { result in
            switch result  {
                
            case .response(let data, _):
                do {
                    let myStruct = try JSONDecoder().decode(type, from: data)
                    
                    completion(.success(myStruct))
                    
                } catch {
                    
                    completion(.error(.badJSON))
                }
            case .error(let error):
                completion(.error(error))
            }
        }
    }
    
    private func requestWith(urlRequest:URLRequest, completion: @escaping(URLResult)->()) {
         
        dataTask?.cancel()
         
        dataTask = defaultSession.dataTask(with:urlRequest) { data, response, error in
             
            if error != nil {
                
                completion(.error(.noConnection))
                return
            }
            
            let responseCode = (response as! HTTPURLResponse).statusCode
           
            if (200..<299).contains(responseCode) {
                completion(.response(data!, response!))
                
                return
                
            } else {
                completion(.error(.apiError(response.debugDescription)))
                return
            }
        }
            dataTask?.resume()
    }
}
