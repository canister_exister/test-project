//
//  SchoolDetailsModel.swift
//  NYCSchools
//
//  Created by Vitalii Miroshnychenko on 5/7/20.
//  Copyright © 2020 Vitalii Miroshnychenko. All rights reserved.
//

import Foundation

struct SchoolDetailsModel: Codable {
    
    let mathScore: String
    let readingScore: String
    let writingScore: String
    let testTakers: String
    
    enum CodingKeys: String, CodingKey {
        case mathScore = "sat_math_avg_score"
        case testTakers = "num_of_sat_test_takers"
        case writingScore = "sat_writing_avg_score"
        case readingScore = "sat_critical_reading_avg_score"
    }
}
