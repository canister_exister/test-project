//
//  SchoolListModel.swift
//  NYCSchools
//
//  Created by Vitalii Miroshnychenko on 5/6/20.
//  Copyright © 2020 Vitalii Miroshnychenko. All rights reserved.
//

import Foundation

struct SchoolListModel: Codable {
    
    let school_name: String
    let dbn: String
    
}
