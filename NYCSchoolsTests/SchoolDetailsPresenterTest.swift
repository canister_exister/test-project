//
//  SchoolDetailsPresenterTest.swift
//  NYCSchoolsTests
//
//  Created by Vitalii Miroshnychenko on 5/8/20.
//  Copyright © 2020 Vitalii Miroshnychenko. All rights reserved.
//

import XCTest
@testable import NYCSchools

class SchoolDetailsPresenterTest: XCTestCase {

    private var sut: SchoolDetailsPresenterProtocol!
    private var mockView: SchoolDetailsVCMock!
    
    override func setUp() {
        super.setUp()

        mockView = SchoolDetailsVCMock()
        sut = SchoolDetailsPresenter(mockView)
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func testCheckForEmptyModel() {
        sut.getDateailsFor(school: "1")
        
        let expectation = XCTestExpectation(description: "Got response")
        mockView.expectation = expectation

        wait(for: [expectation], timeout: 5)
        
        XCTAssert(mockView.invokedShowAlert)
    }
    
    func testScoreUpdate() {
        sut.getDateailsFor(school: "02M047")
    
        let expectation = XCTestExpectation(description: "Got response")
        mockView.expectation = expectation

        wait(for: [expectation], timeout: 5)
    
        XCTAssert(mockView.invokedUpdatedDetailsFor)
        XCTAssertEqual(mockView.mathScore, "400", "score not equal")
        XCTAssertEqual(mockView.writingScore, "387", "score not equal")
        XCTAssertEqual(mockView.readingScore, "395", "score not equal")
    }
}
