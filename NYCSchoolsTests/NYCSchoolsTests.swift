//
//  NYCSchoolsTests.swift
//  NYCSchoolsTests
//
//  Created by Vitalii Miroshnychenko on 5/6/20.
//  Copyright © 2020 Vitalii Miroshnychenko. All rights reserved.
//

import XCTest
@testable import NYCSchools

class NYCSchoolsTests: XCTestCase {
    
    func testRequest() {
        
        let req = ApiEngine()
        
        var score = ""
        
        let exp = expectation(description: "done")
        
        req.createRequest(endPoint: SCHOOL_DETAILS_URL + SCHOOL_NAME + "02M047", type: [SchoolDetailsModel].self) { result in
            
            switch result {
                
            case .success(let success):
                let res = (success as! [SchoolDetailsModel]).first!
                score = res.mathScore
                exp.fulfill()
                XCTAssertEqual(score, "400")
               
            case .error(let error):
                score = error.localizedDescription
                XCTFail()
            }
        }
        wait(for: [exp], timeout: 5)
    }
}
