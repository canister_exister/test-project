//
//  SchoolDetailsVCMock.swift
//  NYCSchoolsTests
//
//  Created by Vitalii Miroshnychenko on 5/8/20.
//  Copyright © 2020 Vitalii Miroshnychenko. All rights reserved.
//

import Foundation
import UIKit
import XCTest
@testable import NYCSchools

class SchoolDetailsVCMock: SchoolDetailsVCProtocol {
    
    var expectation: XCTestExpectation?
    var mathScore = ""
    var readingScore = ""
    var writingScore = ""
    
    var invokedUpdatedDetailsFor = false
    func updatedDetailsFor(math: String, reading: String, writing: String) {
        invokedUpdatedDetailsFor = true
        mathScore = math
        readingScore = reading
        writingScore = writing
        expectation?.fulfill()
    }
    
    var invokedShowAlert = false
    func showAlert() {
        invokedShowAlert = true
        expectation?.fulfill()
    }
}
